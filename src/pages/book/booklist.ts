import { Component } from '@angular/core';
import { ModalController, MenuController } from 'ionic-angular';
import { Book } from '../../app/entities/book';
import { LivreCdService } from '../../app/services/livrecd.service';
import { BooklendPage } from './booklend/booklend';

@Component({
  selector: 'page-booklist',
  templateUrl: 'booklist.html',
})
export class BookListPage {

  booksList: Book[];
  constructor(private modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private bookcdService: LivreCdService) {
  }
  ionViewWillEnter(){
    this.booksList = this.bookcdService.booksList.slice();
  }

  onLoadBook(index:number){
    let modal = this.modalCtrl.create(BooklendPage, {index: index});
    modal.present();
  }
  onToggleMenu(){
    this.menuCtrl.open();
  }


}
