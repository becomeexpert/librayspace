import { Component, OnInit } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Book } from '../../../app/entities/book';
import { LivreCdService } from '../../../app/services/livrecd.service';


@Component({
  selector: 'page-booklend',
  templateUrl: 'booklend.html',
})
export class BooklendPage implements OnInit{

  index: number;
  book: Book;
  constructor(public navParams: NavParams,
    public viewCtrl: ViewController,
    private bookcdService: LivreCdService) {
  }
  ngOnInit(){
    this.index = this.navParams.get('index');
    this.book = this.bookcdService.booksList[this.index];
  }
   dismissModal(){
     this.viewCtrl.dismiss();
   }
   onLend(){
     this.bookcdService.isLend(this.book);
   }
  


}
