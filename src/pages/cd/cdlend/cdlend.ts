import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { CD } from '../../../app/entities/cd';
import { LivreCdService } from '../../../app/services/livrecd.service';

@Component({
  selector: 'page-cdlend',
  templateUrl: 'cdlend.html',
})
export class CdlendPage {

  index: number;
  cd: CD;
  constructor(public navParams: NavParams,
    public viewCtrl: ViewController,
    private bookcdService: LivreCdService) {
  }
  ngOnInit(){
    this.index = this.navParams.get('index');
    this.cd = this.bookcdService.cdList[this.index];
  }
   dismissModal(){
     this.viewCtrl.dismiss();
   }
   onLend(){
     this.bookcdService.isLend(this.cd);
   }
}
