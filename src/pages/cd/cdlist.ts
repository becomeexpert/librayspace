import { Component } from '@angular/core';
import { MenuController, ModalController } from 'ionic-angular';
import { CD } from '../../app/entities/cd';
import { LivreCdService } from '../../app/services/livrecd.service';
import { CdlendPage } from './cdlend/cdlend';


@Component({
  selector: 'page-cdlist',
  templateUrl: 'cdlist.html',
})
export class CdListPage {
  cdsList: CD[];
  constructor(private modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private bookcdService: LivreCdService) {
  }
  ionViewWillEnter(){
    this.cdsList = this.bookcdService.cdList.slice();
  }


  onToggleMenu(){
    this.menuCtrl.open();
  }
  onLoadCd(index: number){
    let modal = this.modalCtrl.create(CdlendPage, {index:index});
    modal.present();
  }
}
