import  { Component, OnInit } from '@angular/core';
import { BookListPage } from '../book/booklist';
import { CdListPage } from '../cd/cdlist';


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit {

  booksPage = BookListPage;
  cdsPage = CdListPage;
  constructor() { }

  ngOnInit() { 

  }

}