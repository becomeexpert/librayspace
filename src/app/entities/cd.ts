export class CD{
    album: string;//le titre du CD
    auteur: string;// l'auteur ou les auteurs
    duree: string;// la durée en minutes
    isLend:boolean;// le cd a été prêté si true et false sinon

    constructor(album:string){
        this.album = album;
        this.isLend=false;

    }
}