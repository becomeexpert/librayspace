export class Book{
    titre:string;// le titre du livre
    nbrePage: number;// le nombre de page ex:50
    auteur: string;// auteur de ce livre
    isbn: string;
    isLend:boolean;// le livre a été prêter si true, sinon false

    constructor(title:string){
        this.titre = title;
        this.isLend = false;

    }
}