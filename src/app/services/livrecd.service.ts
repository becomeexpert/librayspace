import { Book } from "../entities/book";
import { CD } from "../entities/cd";

export class LivreCdService{
    booksList: Book[] = [
        {
            titre: "L’intelligence émotionnelle en leadership",
            nbrePage: 63,
            auteur: "Julian Jencquel",
            isbn: "978-87-403-0607-1",
            isLend: true
        },
        {
            titre: "Comment coacher mes vendeurs",
            nbrePage: 178,
            auteur: "Michel Bélanger",
            isbn: "978-87-403-0558-6",
            isLend: false
        },
        {
            titre: "Le Guide Pratique à la Gestion de Projet",
            nbrePage: 77,
            auteur: "Christine Petersen, PMP",
            isbn: "978-87-403-1658-2",
            isLend: true
        },
        {
            titre: "Gestion du Stress",
            nbrePage: 29,
            auteur: "Cac Consulting - Estelle Coudry",
            isbn: "978-87-403-0507-4",
            isLend: false
        },
        {
            titre: "Développez votre potentiel",
            nbrePage: 69,
            auteur: "Vincent Delourmel",
            isbn: "978-87-403-0529-6",
            isLend: true
        }
        
       
    ];
    cdList: CD[] =[
        {
            album: "Love In The future",
            auteur: "John Legend",
            duree: "46 min",
            isLend: false
        },
        {
            album: "Rocket Man",
            auteur: "Elton John",
            duree: "50 min",
            isLend: true
        },
        {
            album: "WestLife",
            auteur: "WestLife",
            duree: "60 min",
            isLend: false
        },
        {
            album: "Coast To Coast",
            auteur: "WestLife",
            duree: "40 min",
            isLend: false
        },
        {
            album: "The massacre",
            auteur: "50 Cent, OLIVIA",
            duree: "120 min",
            isLend: true
        },
        {
            album: "Drôle de parcours",
            auteur: "La fouine, Zaho",
            duree: "35 min",
            isLend: false
        }      
    ];
    isLend(objet:any){
        objet.isLend=!objet.isLend;
    }
}