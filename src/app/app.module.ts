import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { BooklendPage } from '../pages/book/booklend/booklend';
import { BookListPage } from '../pages/book/booklist';
import { CdlendPage } from '../pages/cd/cdlend/cdlend';
import { CdListPage } from '../pages/cd/cdlist';
import { SettingsPage } from '../pages/settings/settings';
import { LivreCdService } from './services/livrecd.service';
import { TabsPage } from '../pages/tabs/tabs';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    BooklendPage,
    BookListPage,
    CdlendPage,
    CdListPage,
    SettingsPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    BooklendPage,
    BookListPage,
    CdlendPage,
    CdListPage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LivreCdService
  ]
})
export class AppModule {}
